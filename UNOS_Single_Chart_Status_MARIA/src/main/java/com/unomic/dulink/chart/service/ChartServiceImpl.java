package com.unomic.dulink.chart.service;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.unomic.dulink.chart.domain.ChartVo;
 

@Service
@Repository
public class ChartServiceImpl extends SqlSessionDaoSupport implements ChartService{
	
	private final static String namespace= "com.unomic.dulink.chart.";
	private static final Logger logger = LoggerFactory.getLogger(ChartServiceImpl.class);
	
//	@Autowired
//	@Resource(name="sqlSessionTemplate_app_server")
//	private SqlSession app_server_sql;
	
	@Override
	public String getStartTime(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = (String) sql.selectOne(namespace + "getStartTime", chartVo);
		return rtn;
	}
	
	@Override
	public String getComName(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = URLEncoder.encode((String) sql.selectOne(namespace + "getComName", chartVo),"utf-8");
		return str;
	}
	
	
	@Override
	public ChartVo getBanner(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getBanner", chartVo);
		return chartVo;
	}

	@Override
	public String login(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		
		int exist = (int) sql.selectOne(namespace + "login", chartVo);
		
		if(exist==0){
			str = "fail";
		}else{
			str = "success";
		};
		
		return str;
	};
	
//	@Override
//	public String getAppList(ChartVo chartVo) throws Exception {
//		List <ChartVo> dataList  = app_server_sql.selectList(namespace + "getAppList", chartVo);
//	    
//		List list = new ArrayList<ChartVo>();
//		for(int i = 0; i < dataList.size(); i++){
//			Map map = new HashMap();
//			
//			map.put("id", dataList.get(i).getId());
//			map.put("appId", dataList.get(i).getAppId());
//			map.put("name", dataList.get(i).getAppName());
//			map.put("url", dataList.get(i).getUrl());
//			
//			list.add(map);
//		}; 
//  
//		Map dataMap = new HashMap(); 
//		dataMap.put("dataList", list);
//		ObjectMapper om = new ObjectMapper();
//		String str = "";
//		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
//		return str;
//	}

//	@Override
//	public String removeApp(ChartVo chartVo) throws Exception {
//		String str = "";
//		try{
//			app_server_sql.delete(namespace + "removeApp", chartVo);
//			str = "success";
//		}catch(Exception e){
//			e.printStackTrace();
//			str = "fail";
//		}
//		return str;
//	}

//	@Override
//	public String addNewApp(ChartVo chartVo) throws Exception {
//		String str = "";
//		try{
//			app_server_sql.delete(namespace + "addNewApp", chartVo);
//			str = "success";
//		}catch(Exception e){
//			e.printStackTrace();
//			str = "fail";
//		}
//		return str;
//	}
	
	//common func
	@Override
	public String getDvcNameList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getDvcNameList", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < dataList.size(); i++ ){
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getDvcId());
//			map.put("status", dataList.get(i).getStatus());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(),"utf-8"));
//			map.put("lastAlarmCode", dataList.get(i).getLastAlarmCode());
//			map.put("lastAlarmMsg", dataList.get(i).getLastAlarmMsg());
			
			list.add(map);
		};
		
		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list); 
		
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}
	
	@Override
	public ChartVo getCurrentDvcData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getCurrentDvcData", chartVo);
		//chartVo.setEndDateTime((String)sql.selectOne(namespace + "getLastUpdateTime", chartVo));
		 
		if(chartVo==null){
			chartVo = new ChartVo();
			chartVo.setChartStatus("null");
		};

		return chartVo;
	};
	
	@Override
	public String getTimeData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> statusList = sql.selectList(namespace +"getTimeData", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < statusList.size(); i++){
			Map map = new HashMap();
			map.put("status", statusList.get(i).getStatus());
			map.put("startDateTime", statusList.get(i).getStartDateTime());
			map.put("spdLoad", statusList.get(i).getSpdLoad());
			map.put("spdOverride", statusList.get(i).getFeedOverride());
			
			list.add(map);
		};
		
		Map statusMap = new HashMap();
		statusMap.put("statusList", list);
		
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(statusMap);
		return str;
	}
	
	@Override
	public String getOldTimeData(ChartVo chartVo) throws Exception {
		
		SqlSession sql = getSqlSession();
		List <ChartVo> statusList = sql.selectList(namespace +"getOldTimeData", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < statusList.size(); i++){
			Map map = new HashMap();
			map.put("status", statusList.get(i).getStatus());
			map.put("startDateTime", statusList.get(i).getStartDateTime());
			map.put("spdLoad", statusList.get(i).getSpdLoad());
			map.put("spdOverride", statusList.get(i).getFeedOverride());
			
			list.add(map);
		};
		
		Map statusMap = new HashMap();
		statusMap.put("statusList", list);
		
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(statusMap);
		return str;
	}
	
	@Override
	public String getDetailBlockData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getDetailBlockData", chartVo);
		
		List list = new ArrayList(); 
		for(int i = 0; i < dataList.size(); i++ ){
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("lastAlarmCode", dataList.get(i).getLastAlarmCode());
			map.put("lastAlarmMsg", dataList.get(i).getLastAlarmMsg());
			map.put("lastTgPrdctNum", dataList.get(i).getLastTgPrdctNum());
			map.put("lastFnPrdctNum", dataList.get(i).getLastFnPrdctNum());
			map.put("LastAvrCycleTime", dataList.get(i).getLastAvrCycleTime());
			map.put("tgCnt", dataList.get(i).getTgCnt());
			map.put("status", dataList.get(i).getStatus());
			map.put("prdctPerHour", dataList.get(i).getPrdctPerHour());
			
			map.put("prdctPerCyl", dataList.get(i).getPrdctPerCyl());
			//map.put("remainCnt", dataList.get(i).getRemainCnt());
			map.put("feedOverride", dataList.get(i).getFeedOverride());
			map.put("spdLoad", dataList.get(i).getSpdLoad());
			map.put("ncAlarmNum1", dataList.get(i).getNcAlarmNum1());
			map.put("ncAlarmMsg1", URLEncoder.encode(dataList.get(i).getNcAlarmMsg1(),"utf-8"));
			map.put("ncAlarmNum2", dataList.get(i).getNcAlarmNum2());
			map.put("ncAlarmMsg2", URLEncoder.encode(dataList.get(i).getNcAlarmMsg2(),"utf-8"));
			map.put("ncAlarmNum3", dataList.get(i).getNcAlarmNum3());
			map.put("ncAlarmMsg3", URLEncoder.encode(dataList.get(i).getNcAlarmMsg3(),"utf-8"));
		
			 
			map.put("programHeader", dataList.get(i).getProgramHeader());
			map.put("programName", dataList.get(i).getProgramName());
			map.put("type", dataList.get(i).getType());
			list.add(map);
		};
		
		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);
		
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getAlarmList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = sql.selectList(namespace + "getAlarmList", chartVo);
		
		List list = new ArrayList(); 
		for(int i = 0; i < dataList.size(); i++ ){
			Map map = new HashMap();

			
			map.put("startDateTime", dataList.get(i).getStartDateTime());
			map.put("ncAlarmNum1", dataList.get(i).getNcAlarmNum1());
			map.put("ncAlarmMsg1", URLEncoder.encode(dataList.get(i).getNcAlarmMsg1(),"utf-8"));
			map.put("ncAlarmNum2", dataList.get(i).getNcAlarmNum2());
			map.put("ncAlarmMsg2", URLEncoder.encode(dataList.get(i).getNcAlarmMsg2(),"utf-8"));
			map.put("ncAlarmNum3", dataList.get(i).getNcAlarmNum3());
			map.put("ncAlarmMsg3", URLEncoder.encode(dataList.get(i).getNcAlarmMsg3(),"utf-8"));
			 
			list.add(map);
		};
		
		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);
		
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}
};