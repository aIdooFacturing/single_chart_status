<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<spring:message code="device" var="device"></spring:message>
<spring:message code="cycle" var="cycle"></spring:message>
<spring:message code="ophour" var="ophour"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
 <script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script> 

<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
 <script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script> 
<script src="${ctxPath }/js/solid-gauge.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.circle-diagram.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery-rotate.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	
	var dvcIdx = 0;
	var first = true;
	function getDvcList(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		
		var url =  ctxPath + "/getDvcNameList.do";
		var param = "shopId=" + shopId + 
					"&line=ALL" + 
					"&sDate=" + today + 
					"&eDate=" + today + " 23:59:59" + 
					"&fromDashboard=" + fromDashboard;
		
		dvcArray = [];
		// console.log("list 확인");
		// console.log(param);
		$.ajax({
			url : url,
			data :param,
			dataType : "json",
			type : "post",
			success : function(data){
				// console.log(data);
				var json = data.dataList;
				
				//var list = "<option value='-1'>Auto</option>";
				var list = "";
				
				$(json).each(function(idx, data){
					dvcArray.push(data.dvcId)
					list += "<option value='" + data.dvcId + "'>" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</option>";					
				});

				$("#dvcId").html(list);
				// console.log("list 확인됨");
				//console.log(list);
				
				$("#dvcId").css({
					"font-size" : getElSize(50),
					//"margin-bottom" : getElSize(10),
					"width" : getElSize(500),
					"-webkit-appearance": "none",
				    "-moz-appearance": "none",
			    	"appearance": "none",
			    	//"margin-top" : getElSize(100)
				});
				
				/* if(first){
					//localstorage에 저장 된 dvcid get
					for(var i = 0; i < dvcArray.length; i++){
						if(selectedDvcId == dvcArray[i]){
							dvcIdx = i;
						}
					}
					
					first = false;
				} */
				
				if(selectedDvcId==null){
					dvcId = dvcArray[0];	
				}else{
					dvcId = selectedDvcId
				}
				
				$("#dvcId").val(dvcId);
				
				dvcIdx++;
				if(dvcIdx>=dvcArray.length) dvcIdx=0;
				
				getStartTime();
			}
		});
	};
	
	function getCurrentDvcStatus(dvcId){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var today = $("#today").val();
		
		var url = "${ctxPath}/getCurrentDvcData.do";
		var param = "dvcId=" + dvcId + 
					"&workDate=" + today;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var chartStatus = data.chartStatus;
				var type = data.type;
				var progName = data.lastProgramName;
				var progHeader = data.lastProgramHeader;
				var name = data.name;
				var inCycleTime = data.inCycleTime;
				var cuttingTime = data.cuttingTime;
				var waitTime = data.waitTime;
				var alarmTime = data.alarmTime;
				var noConTime = data.noConnectionTime;
				var spdLoad = data.spdLoad;
				var feedOverride = data.feedOverride;
				var opRatio = data.opRatio;
				var cuttingRatio = data.cuttingRatio;
				var alarm = data.alarm;
				var lastProgramName = data.lastProgramName;
				var lastProgramHeader = data.lastProgramHeader;
				var last_chart_status = data.chartStatus;
				
				if(lastProgramName==null)lastProgramName = "-"
				if(lastProgramHeader==null)lastProgramHeader = "-"
					
				var prgName = "[" + lastProgramName + "] " + lastProgramHeader;
				
				$("#programName").html(prgName).css({
					"color" : "white",
					"font-size" :getElSize(70),
					"position" : "absolute",
					"z-index" : 9,
					"top" : getElSize(250) + marginHeight,
					"left" : getElSize(2345) + marginWidth
				});
				
				
				$("#program_div").html(prgName).css({
					"color" : "white",
					"font-size" :getElSize(50),
					"margin" : getElSize(20) + "px"
				});
				
				if(data.chartStatus=="null"){
					name = window.sessionStorage.getItem("name");
				}
				
				
				//spd, feed
				$("#opratio_gauge").empty();
				$("#opratio_gauge").circleDiagram({
					textSize: getElSize(50), // text color
					percent : Math.round(opRatio) + "%",
					size: getElSize(200), // graph size
					borderWidth: getElSize(20), // border width
					bgFill: "#353535", // background color
					frFill: "#0080FF", // foreground color
					//font: "serif", // font
					textColor: '#0080FF' // text color
				});
				
				$("#cutting_gauge").empty();
				$("#cutting_gauge").circleDiagram({
					textSize: getElSize(50), // text color
					percent : Math.round(cuttingRatio) + "%",
					size: getElSize(200), // graph size
					borderWidth: getElSize(20), // border width
					bgFill: "#353535", // background color
					frFill: "#0080FF", // foreground color
					//font: "serif", // font
					textColor: '#0080FF' // text color
				});
						
				
				$("#opratio_span").html("${op_ratio}").css({
					"top" : getElSize(1780),
					"color" : "#8D8D8D",
					"left" :$("#opratio_gauge").offset().left + ($("#cutting_gauge").width()/2) - ($("#opratio_span").width()/2)- marginWidth
				});
				
				//io logik이 아닐 때
				if(String(type).indexOf("IO")==-1){
					$("#cutting_gauge, #cuttingratio_span").css({"display": "block"})
					$("#cuttingratio_span").html("${cuttingratio}").css({
						"top" : getElSize(1780),
						"color" : "#8D8D8D",
						"left" :$("#cutting_gauge").offset().left + ($("#cutting_gauge").width()/2) - ($("#cuttingratio_span").width()/2)- marginWidth
					});
				}else{
					$("#cutting_gauge, #cuttingratio_span").css({"display": "none"})
				}
				
				var alarm_time = (alarmTime/60/60).toFixed(1);
				if(alarmTime!=0 && alarm_time == "0.0"){
					alarm_time = "0.1";
				}
				
				$("#inCycleSpan").html((inCycleTime/60/60).toFixed(1));
				$("#cutSpan").html((cuttingTime/60/60).toFixed(1));
				$("#waitSpan").html((waitTime/60/60).toFixed(1));
				$("#alarmSpan").html(alarm_time);
				$("#noConnSpan").html((noConTime/60/60).toFixed(1));
				
				
				/* var incycleTime = Number((inCycleTime/60/60).toFixed(1)) - Number((cuttingTime/60/60).toFixed(1))
				var cutTime = Number((cuttingTime/60/60).toFixed(1))
				
				if(Number(cuttingTime/60/60).toFixed(1)==0){
					cutTime = 0
					incycleTime = Number((inCycleTime/60/60).toFixed(1))
				} */
				
				pieChart.series[0].data[0].update(Number((inCycleTime/60/60).toFixed(1)))	//inCycleTime
				//pieChart.series[0].data[1].update(Number((cuttingTime/60/60).toFixed(1)))	//cuttingTime
				pieChart.series[0].data[1].update(Number((waitTime/60/60).toFixed(1)))
				pieChart.series[0].data[2].update(Number(alarm_time))
				pieChart.series[0].data[3].update(Number((noConTime/60/60).toFixed(1)))
				
				
				pieCuttingChart.series[0].data[0].update( Number((cuttingTime/60/60).toFixed(1)) )	//cuttingTime
				pieCuttingChart.series[0].data[1].update( Number((inCycleTime/60/60).toFixed(1)) - Number((cuttingTime/60/60).toFixed(1))) // inCycleTime - cuttingTime
				pieCuttingChart.series[0].data[2].update(Number((waitTime/60/60).toFixed(1)))
				pieCuttingChart.series[0].data[3].update(Number(alarm_time))
				pieCuttingChart.series[0].data[4].update(Number((noConTime/60/60).toFixed(1)))
				
				
				// 초기화 
				for(var i = 0; i < pieChartStatus.series[0].data.length; i++){
					pieChartStatus.series[0].data[i].update(0)
				}

				if(last_chart_status == "IN-CYCLE" && spdLoad > 0){
					pieChartStatus.series[0].data[0].update(1)
				}else if(last_chart_status == "IN-CYCLE"){
					pieChartStatus.series[0].data[1].update(1)	
				}else if(last_chart_status == "WAIT"){
					pieChartStatus.series[0].data[2].update(1)
				}else if(last_chart_status == "ALARM"){
					pieChartStatus.series[0].data[3].update(1)	
				}else if(last_chart_status == "NO-CONNECTION"){
					pieChartStatus.series[0].data[4].update(1)	
				}else{
					pieChartStatus.series[0].data[2].update(1)
				}
				
				getDetailData();
				
				console.log("TIME : " + getToday());
				
			}
		});
	};
	
	var handle = 0;
	var selectedDvcId;
	
	function drawGauge(){
		$("#gauge").css({
			"width" : getElSize(900),
			"height" : getElSize(380)
		})
		
		var gaugeOptions = {

			    chart: {
			        type: 'solidgauge',
			        backgroundColor : "rgba(0,0,0,0)",
			        marginBottom : 0,
			        marginTop : -getElSize(460)
			        
			    },

			    
			    title: null,

			    pane: {
			        center: ['50%', '100%'],
			        size: '90%',
			        startAngle: -90,
			        endAngle: 90,
			        background: {
			            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
			            innerRadius: '60%',
			            outerRadius: '100%',
			            shape: 'arc'
			        }
			    },

			    tooltip: {
			        enabled: false
			    },

			    // the value axis
			    yAxis: {
			        stops: [
			            /* [0.1, '#55BF3B'], // green
			            [0.5, '#DDDF0D'], // yellow
			            [0.9, '#DF5353'] // red */
			            [1, '#55BF3B'], // green
			        ],
			        lineWidth: 0,
			        minorTickInterval: null,
			        tickAmount: 2,
			        title: {
			            y: -70
			        },
			        labels: {
			            y: 16
			        }
			    },

			    plotOptions: {
			        solidgauge: {
			            dataLabels: {
			                y: 5,
			                borderWidth: 0,
			                useHTML: true
			            }
			        }
			    }
			};

			// The speed gauge
			/* $('#gauge').highcharts(Highcharts.merge(gaugeOptions, {
			//var chartSpeed = Highcharts.chart('gauge', Highcharts.merge(gaugeOptions, {
			    yAxis: {
			    	min: 0,
					max: 100,
			        title: {
			            text: ''
			        }
			    },

			    credits: {
			        enabled: false
			    },

			    series: [{
			        name: 'Speed',
			        data: [0],
			        dataLabels: {
			            format: '<div style="text-align:center"><font style="font-size:25px; color:' +
			                ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'blue') + '">{y}%</font>'
			        },
			        tooltip: {
			            valueSuffix: ' km/h'
			        }
			    }]

			})); */
	};
	
	function changeDateVal(){
		window.sessionStorage.setItem("date", $("#today").val())		
		window.sessionStorage.setItem("dvcId", $("#dvcId").val());
		
		console.log("changed")
		
		var now = new Date(); 
		var todayAtMidn = new Date(now.getFullYear(), now.getMonth(), now.getDate());
		
		var date = new Date();
		
		var year = date.getFullYear();
		var month = date.getMonth()+1;
		var day = date.getDate();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		// Set specificDate to a specified date at midnight.
		
		var selectedDate = $("#today").val();
		var s_year = selectedDate.substr(0,4);
		var s_month = selectedDate.substr(5,2);
		var s_day = selectedDate.substr(8,2);

		var specificDate = new Date(s_month + "/" + s_day + "/" + s_year);
		
		var time = year + "-" + month + "-" + day;
		
		var today = getToday().substr(0,10);
		
		//오후 6시 이후 날짜+1
		/* if(new Date().getHours() >= startHour){
			todayAtMidn.setDate(todayAtMidn.getDate() + 1)
			today = todayAtMidn.getFullYear() + "-" + addZero(String(todayAtMidn.getMonth()+1)) + "-" + addZero(String(todayAtMidn.getDate()))
		} */

		// 시간 비교하는 로직 변경 추가
		// 먼저 금일날짜 가져오기
		var date = new Date();
/* 		var year = date.getFullYear();
		var month = addZero(String(date.getMonth() + 1));
		var day = addZero(String(date.getDate())); */
		
		var hourChk = date.getHours();
		var minuteChk = addZero(String(date.getMinutes())).substr(0,2);
		
// 		console.log("===============")
// 		console.log(today)
// 		console.log(startHour,startMinute)
// 		console.log(hourChk,minuteChk)
// 		console.log("===============")
// 		if()
		
	
		//시작 시간 = 종료시간 이니까
		// 시작시간(종료시간) 보다 크고 24시 보다 작을때 +1
		
		//기준 날짜 데이터 변수
		var ChkDate;
		//리턴시 기존 날짜로 리턴하기위한 변수
		//var currentDate = $("#today").val();
		
		/* if(moment().format("HH") >= startHour){
			ChkDate = moment().add("day",1).format("YYYY-MM-DD")
		}else{
			ChkDate = moment().format("YYYY-MM-DD")
		} */
		
		ChkDate = moment().format("YYYY-MM-DD")
		
// 		console.log(ChkDate + " , " + $("#today").val())
// 		console.log(ChkDate < $("#today").val())
		
		if(ChkDate < $("#today").val()){
			alert("${date_warning}");
// 			alert("오늘 이후의 날짜는 선택할 수 없습니다.");
			$("#today").val(ChkDate);
			window.sessionStorage.setItem("date", ChkDate)		

			return;
		}

// 		return;
		
// 		if(todayAtMidn.getTime()<specificDate.getTime()){
// 			alert("오늘 이후의 날짜는 선택할 수 없습니다.");
// 			$("#today").val(today);
// 			return;
// 		};
		
		drawBarChart("timeChart", $("#today").val());
		getDetailData();
		getCurrentDvcStatus(dvcId);

	};
	
	function upDate(){
		var $date = $("#today").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() + 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#today").val(today);
		//changeDateVal();
	};
	
	var tmpPoint = [];
	for(var i = 0; i < 720; i++){
		tmpPoint.push(-10)
	}
	function downDate(){
		var $date = $("#today").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() - 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#today").val(today);
		//changeDateVal();
	};
	
	var appId = 31;
	
	var addAppFlag = true;
	var categoryId = 1;
	
	//var appServerUrl = "http://localhost:8080/App_Store/index.do";
// 	var appServerUrl = "http://52.169.201.78:8080/App_Store/index.do?categoryId=1";
// 	window.addEventListener("message", receiveMessage, false);
	
// 	function receiveMessage(evet){
// 		var msg = event.data;
		
// 		$("#app_store_iframe").animate({
// 			"left" : - $("#app_store_iframe").width() * 1.5 
// 		}, function(){
// 			$("#app_store_iframe").css({
// 				"left" : originWidth,
// 				"top" : (originHeight/2) - ($("#app_store_iframe").height()/2) 
// 			})
			
// 			addAppFlag = true;
// 			$(".addApp").rotate({
// 				duration:500,
// 			    angle: 45,
// 			   	animateTo:0
// 			});
// 		});
		
// 		fileDown(msg.url, msg.appName, msg.appId)
// 	}
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function fileDown(appUrl, appName, appId){
		var url = "${ctxPath}/fileDown.do";
		var param = "fileLocation=" + appUrl + 
					"&fileName=" + appName + 
					"&appId=" + appId + 
					"&ty=" + categoryId + 
					"&shopId=" + shopId;

		getProgress();
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				if(data=="success"){
					getAppList()					
				}
			}, error : function(e1,e2,e3){
				console.log(e1,e2,e3)
			}
		});
	};
	
	var progressLoop = false;
	function getProgress(){
		var url = "${ctxPath}/getProgress.do";
		
		$.ajax({
			url : url,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data<100){
					$(".progressBar").remove();
					//draw ProgressBar();
					var targetBar = $(".nav_span").next("img").width();
					var currentBar = targetBar * Number(data).toFixed(1)/100;
					
					
					var barHeight = getElSize(50);
					var _top = $(".nav_span:nth(" + appCnt + ")").parent("td").offset().top + $(".nav_span:nth(" + appCnt + ")").parent("td").height() - barHeight;
					var _left = $(".nav_span:nth(" + appCnt + ")").parent("td").offset().left;
					
					var bar = document.createElement("div");
					bar.setAttribute("class", "progressBar");
					bar.style.cssText = "width : " + currentBar + "px;" + 
										"height : " + barHeight + "px;" +
										"position : absolute;" + 
										"top : " + _top + "px;" + 
										"color : #8D8D8D;" +
										"left : " + _left + "px;" +
										"z-index : 99999;" + 
										"background-color : lightgreen;";
										
										
					$("body").prepend(bar);
					
					clearInterval(progressLoop);
					progressLoop = setTimeout(getProgress,500)
					
					$(".nav_span:nth(" + appCnt + ")").html(Number(data).toFixed(1) + "%")	 
				}else{
					resetProgress();
				}
				
				$(".addApp").remove();
			}
		});
	};
	
	function resetProgress(){
		var url = "${ctxPath}/resetProgress.do";
		
		$.ajax({
			url : url,
			type : "post",
			success : function(data){
			}
		});
	};
	
	
	function showMinusBtn(parentTd, appId){
		$(".minus").animate({
			"width" : 0
		}, function(){
			$(this).remove();
		});
		
		var img = document.createElement("img");
		img.setAttribute("src", "${ctxPath}/images/minus.png");
		img.setAttribute("class", "minus");
		img.style.cssText = "position : absolute;" +
							"cursor : pointer;" + 
							"z-index: 9999;" + 
							"left : " + (parentTd.offset().left + parentTd.width()) + "px;" +
							"top : " + (parentTd.offset().top) + "px;" +
							"width : " + getElSize(0) + "px;";
		
		$(img).click(function(){
			$("#delDiv").animate({
				"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
				"top" : (window.innerHeight/2) - ($("#delDiv").height()/2) + getElSize(100)
			}, function(){
				$("#delDiv").animate({
					"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
					"top" : (window.innerHeight/2) - ($("#delDiv").height()/2)
				}, 100)
			});
			
			$("#delDiv div:nth(0)").click(function(){
				removeApp(appId);
			})
		});
		
		$(img).animate({
			"width" : getElSize(80)
		});		
		
		$("body").prepend(img)					
	};
	
	function removeApp(appId){
		var url = "${ctxPath}/removeApp.do";
		var param = "appId=" + appId + 
					"&shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					$("#delDiv").animate({
						"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
						"top" : - $("#delDiv").height() * 2
					})	
					
					$(".minus").animate({
						"width" : 0
					}, function(){
						$(this).remove();
					});
					getAppList();
				}
			}
		});
	};
	
	var appArray = [];
	var appCnt = 0;
	function getAppList(){
		clearInterval(progressLoop);
		$(".progressBar").remove();
		
		var url = "${ctxPath}/getAppList.do";
		var param = "categoryId=" + categoryId + 
					"&shopId=" +shopId;
		
		appArray = [];
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
		
				$(".nav_span").html("");
				$(".addApp").remove();
				
				//id, appId, name
				
				appCnt = json.length;
				
				$(".nav_span").next("img").unbind("contextmenu").attr("src", "${ctxPath}/images/unselected.png");
				$(".nav_span").unbind("contextmenu");
				
				$(json).each(function(idx, data){
					var appName;
					if(data.name=="DashBoard_EMO"){
						appName = "${dashboard_emo}"
					}else if(data.name=="DashBoard_Doosan"){
						appName = "${dashboard_doosan}"
					}else if(data.name=="DashBoard_BK"){
						appName = "${dashboard_bk}";
					}else if(data.name=="DashBoard2_BK"){
						appName = "${dashboard_bk2}";
					}else if(data.name=="Single_Chart_Status"){
						appName = "${dailydevicestatus}";
					}else if(data.name=="Production_Board"){
						appName = "${prdct_board}";
					}else if(data.name=="24hChart"){
						appName = "${barchart}";
					}else if(data.name=="DMM"){
						appName = "${dmm}";
					}
					
					appArray.push(data.appId)
					$(".nav_span:nth(" + idx + ")").attr("appId", data.appId);
					$(".nav_span:nth(" + idx + ")").html(appName);
					$(".nav_span:nth(" + idx + ")").click(function(){
						location.href = "http://" + data.url + "?lang=" + window.localStorage.getItem("lang");
					});
					
					$(".nav_span:nth(" + idx + ")").next("img").click(function(){
						/* if(data.name=="DashBoard"){
							location.href = "/DIMF/chart/main.do?categoryId=1";	
						}else{
							location.href = "/" + data.name + "/index.do";	
						} */
							
						location.href = "http://" + data.url + "?lang=" + window.localStorage.getItem("lang");
					});
					
					$(".nav_span:nth(" + idx + ")").next("img").attr("appId", data.appId)
					
					if(data.appId==appId){
						$(".nav_span:nth(" + idx + ")").next("img").attr("src", "${ctxPath}/images/selected_blue.png")
						$(".nav_span:nth(" + idx + ")").css("color", "white");
					}
					$(".nav_span:nth(" + idx + ")").next("img").contextmenu(function(e){
						e.preventDefault();
						var appId = $(this).prev("span").attr("appId");
						var parentTd = $(".nav_span:nth(" + idx + ")").parent("Td");
						showMinusBtn(parentTd, appId)						
					});
					
					$(".nav_span:nth(" + idx + ")").contextmenu(function(e){
						e.preventDefault();
						var appId = $(this).attr("appId");
						var parentTd = $(".nav_span:nth(" + idx + ")").parent("Td")
						showMinusBtn(parentTd, appId)						
					});
				});
				
				var totalCell = 10;
				
				var parentTd = $(".nav_span:nth(" + json.length + ")").parent("td");
				var img = document.createElement("img");
				img.setAttribute("src", "${ctxPath}/images/add.png");
				img.setAttribute("class", "addApp");
				img.style.cssText = "position : absolute;" +
									"cursor : pointer;" + 
									"z-index: 9999;" + 
									"left : " + (parentTd.offset().left + (parentTd.width()/2) - getElSize(40)) + "px;" +
									"top : " + (parentTd.offset().top + (parentTd.height()/2) - getElSize(40)) + "px;" +
									"width : " + getElSize(80) + "px;"; 
									
					
				$("body").prepend(img);
				
				//iframe 호출
				$(img).click(function(){
					document.querySelector("#app_store_iframe").contentWindow.postMessage(appArray, '*');
					
					if(addAppFlag){
						$(this).rotate({
					   		duration:500,
					      	angle: 0,
					      	animateTo:45
						});
						$("#app_store_iframe").animate({
							"left" : (originWidth/2) - ($("#app_store_iframe").width()/2)
						});	
					}else{
						$(this).rotate({
							duration:500,
						    angle: 45,
						   	animateTo:0
						});
						$("#app_store_iframe").animate({
							"left" : - $("#app_store_iframe").width() * 1.5 
						}, function(){
							$("#app_store_iframe").css({
								"left" : originWidth,
								"top" : (originHeight/2) - ($("#app_store_iframe").height()/2) 
							})
						});
					}
					
					addAppFlag = !addAppFlag;
				})
			}
		});
	};
	
	
	
	var sessionDate;
	var pieChart;
	var pieCuttingChart;
	var pieChartStatus;
	
	function drawPieChart(){
		  // Build the chart
	    $('#pieChart').highcharts({
	        chart: {
	            plotBackgroundColor: null,
	            width : getElSize(350),
	            height : getElSize(350),
	            plotBorderWidth: null,
	            backgroundColor : "rgba(255,255,255,0)",
	            plotShadow: false,
	            type: 'pie'
	        },
	        title: {
	            text: null
	        },
	        tooltip: {
				enabled :false,
	        	pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	        },
	        legend : {
	        	enabled : false
	        },
	        exporting : false,
	        credits : false,
	        plotOptions: {
	            pie: {
	            	size: getElSize(350),
	            	borderWidth: 0,
	                //allowPointSelect: true,
	                cursor: 'pointer',
	                dataLabels: {
	                    enabled: false
	                },
	                showInLegend: true
	            }
	        },
	        series: [{
	            name: 'Brands',
	            colorByPoint: true,
	            data: [{
	                name: 'In-Cycle',
	                y: 0,
	                color : incycleColor
	            }, /* {
	                name: 'Cut',
	                y: 0,
	                color : cuttingColor,
	            }, */ {
	                name: 'Wait',
	                y: 0,
	                color : waitColor
	            }, {
	                name: 'Alarm',
	                y: 0,
	                color : alarmColor
	            }, {
	                name: 'Off',
	                y: 0,
	                color : noconnColor
	            }]
	        }]
	    });
		  
	    pieChart = $('#pieChart').highcharts()
	    
	    
	    $('#pieCuttingChart').highcharts({
	        chart: {
	            plotBackgroundColor: null,
	            width : getElSize(350),
	            height : getElSize(350),
	            plotBorderWidth: null,
	            backgroundColor : "rgba(255,255,255,0)",
	            plotShadow: false,
	            type: 'pie'
	        },
	        title: {
	            text: null
	        },
	        tooltip: {
				enabled :false,
	        	pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	        },
	        legend : {
	        	enabled : false
	        },
	        exporting : false,
	        credits : false,
	        plotOptions: {
	            pie: {
	            	size: getElSize(246),
	            	borderWidth: 0,
	                //allowPointSelect: true,
	                cursor: 'pointer',
	                dataLabels: {
	                    enabled: false
	                },
	                showInLegend: true
	            }
	        },
	        series: [{
	            name: 'Brands',
	            colorByPoint: true,
	            /* data: [{
	                name: 'Cut',
	                y: 0,
	                color : cuttingColor
	            }, {
	                name: 'In-Cycle',
	                y: 0,
	                color : '#C4C5C5'//incycleColor
	            }] */
	            data: [{
	                name: 'Cut',
	                y: 0,
	                color : cuttingColor,
	            }, {
	                name: 'In-Cycle',
	                y: 0,
	                color : incycleColor
	            }, {
	                name: 'Wait',
	                y: 0,
	                color : waitColor
	            }, {
	                name: 'Alarm',
	                y: 0,
	                color : alarmColor
	            }, {
	                name: 'Off',
	                y: 0,
	                color : noconnColor
	            }]
	        }]
	    });
		  
	    pieCuttingChart = $('#pieCuttingChart').highcharts();
	    
	    
	 	// pieChartStatus
	    $('#pieChartStatus').highcharts({
	        chart: {
	            plotBackgroundColor: null,
	            width : getElSize(352),
	            height : getElSize(350),
	            plotBorderWidth: null,
	            backgroundColor : "rgba(255,255,255,0)",
	            plotShadow: false,
	            type: 'pie'
	        },
	        title: {
	            text: null
	        },
	        tooltip: {
				enabled :false,
	        	pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	        },
	        legend : {
	        	enabled : false
	        },
	        exporting : false,
	        credits : false,
	        plotOptions: {
	            pie: {
	            	size: getElSize(115),
	            	borderWidth: 0,
	                //allowPointSelect: true,
	                cursor: 'pointer',
	                dataLabels: {
	                    enabled: false
	                },
	                showInLegend: true
	            }
	        },
	        series: [{
	            name: 'Brands',
	            colorByPoint: true,
	            
	            data: [{
	                name: 'Cut',
	                y: 0,
	                color : cuttingColor,
	            }, {
	                name: 'In-Cycle',
	                y: 0,
	                color : incycleColor
	            }, {
	                name: 'Wait',
	                y: 0,
	                color : waitColor
	            }, {
	                name: 'Alarm',
	                y: 0,
	                color : alarmColor
	            }, {
	                name: 'Off',
	                y: 0,
	                color : noconnColor
	            }]
	        }]
	    });
		  
	    pieChartStatus = $('#pieChartStatus').highcharts();
	};
	
	
	
	$(function(){
		selectedDvcId = window.sessionStorage.getItem("dvcId");
		sessionDate = window.sessionStorage.getItem("date");
		
		if(sessionDate==null || sessionDate > moment().format("YYYY-MM-DD")){
			var date = new Date();
			var year = date.getFullYear();
			var month = date.getMonth();
			var day = date.getDate();
			
			var hour = date.getHours();
			
			sessionDate = getToday().substr(0,10)
		};				
		
		$("#today").val(sessionDate);
		getDvcList()
		drawPieChart();		
		//getAppList();
		
		$("#today").change(changeDateVal);
		$("#up").click(upDate);
		$("#down").click(downDate);
		

		//if(selectedDvcId==null) selectedDvcId =96; 
		
		createNav("monitor_nav", 1)
		time();
		
		$("#dvcId").change(function(){
			dvcId = $("#dvcId").val();
			window.localStorage.setItem("dvcId", dvcId)
			if(this.value==-1){
				rotate_flag = true;	
			}else{
				rotate_flag = false;
				//getCurrentDvcStatus(dvcId);
				//changeDateVal()
			}
		});
	
		setDivPos();
		
		/* window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10); */
		
		
		chkBanner();
	});
	
	var startTimeLabel = new Array();
	var startHour, startMinute;
	var now = new Date();
	var timerId_1 = null;
	var timerId_2 = null;
	//var targetMinute = String(addZero(new Date().getMinutes())).substr(0,1);
	
	
	function getStartTime(){
		var url = ctxPath + "/getStartTime.do";
		var param = "shopId=" + shopId;;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "text",
			type : "post",
			success : function(data){
				startHour = data.split("-")[0]
				startMinute = data.split("-")[1].concat('0');

				

				var date = new Date();
				var year = date.getFullYear();
				var month = addZero(String(date.getMonth() + 1));
				var day = addZero(String(date.getDate()));
				var hour = date.getHours();
				var minute = addZero(String(date.getMinutes())).substr(0,2);
				
// 				console.log("현재 날짜 : " + date, year, month, day); 
// 				console.log("날짜 비교 : " + hour, startHour, minute, startMinute);
				
				if(hour < startHour || (hour <= startHour && minute < startMinute)){

// 					console.log("-----------------------------")
					day = addZero(String(new Date().getDate()+1));
					//add cannon
					var $date = $("#today").val();
					var year = $date.substr(0,4);
					var month = $date.substr(5,2);
					var day = $date.substr(8,2);
					
					var current_day = month + "/" + day + "/" + year;
					
// 					console.log("#today : " + $("#today").val());
// 					console.log("cur : " + current_day);
					
					var date = new Date(current_day);
					date.setDate(date.getDate() - 1);
					
					var year = date.getFullYear();
					var month = addZero(String(date.getMonth()+1));
					var day = addZero(String(date.getDate()));
					
					var today = year + "-" + month + "-" + day;
					
// 					console.log("tdy : " + today);
					
					$("#today").val(today);
// 					console.log("-----------------------------")

					// end cannon
									
					// todayAtMidn = new Date(now.getFullYear(), now.getMonth(), now.getDate()+1);
					// console.log("todayAtMidn : "+ todayAtMidn);
				};
								
				var today = year + "-" + month + "-" + day;
				
				
				//getCurrentDvcStatus(dvcId)
				
				getCurrentDvcStatus(dvcId);
				
				timerId_1 = setInterval(function(){
					console.log("dvcId : " + dvcId);
					getCurrentDvcStatus(dvcId);
				}, 1000*60*1)
				

				drawBarChart("timeChart", sessionDate);
				
				timerId_2 = setInterval(function(){
					drawBarChart("timeChart", sessionDate);
				},1000*60*1)
				
			}, error : function(e1,e2,e3){
				console.log(e1,e2,e3)
			}
		});	
	};
	
	function drawBarChart(id, today) {
		var m00 = "",
			m02 = "",
			m04 = "",
			m06 = "",
			m08 = "",
			
			m10 = "";
			m12 = "",
			m14 = "",
			m16 = "",
			m18 = "",
			
			m20 = "";
			m22 = "",
			m24 = "",
			m26 = "",
			m28 = "",
			
			m30 = "";
			m32 = "",
			m34 = "",
			m36 = "",
			m38 = "",
			
			m40 = "";
			m42 = "",
			m44 = "",
			m46 = "",
			m48 = "",
			
			m50 = "";
			m52 = "",
			m54 = "",
			m56 = "",
			m58 = "";
			
		var n = Number(startHour);
		if(startMinute!=0) n+=1;
		
		for(var i = 0, j = n ; i < 24; i++, j++){
			eval("m" + startMinute + "=" + j);
			
			startTimeLabel.push(m00);
			startTimeLabel.push(m02);
			startTimeLabel.push(m04);
			startTimeLabel.push(m06);
			startTimeLabel.push(m08);
			
			startTimeLabel.push(m10);
			startTimeLabel.push(m12);
			startTimeLabel.push(m14);
			startTimeLabel.push(m16);
			startTimeLabel.push(m18);
			
			startTimeLabel.push(m20);
			startTimeLabel.push(m22);
			startTimeLabel.push(m24);
			startTimeLabel.push(m26);
			startTimeLabel.push(m28);
			
			startTimeLabel.push(m30);
			startTimeLabel.push(m32);
			startTimeLabel.push(m34);
			startTimeLabel.push(m36);
			startTimeLabel.push(m38);
			
			startTimeLabel.push(m40);
			startTimeLabel.push(m42);
			startTimeLabel.push(m44);
			startTimeLabel.push(m46);
			startTimeLabel.push(m48);
			
			startTimeLabel.push(m50);
			startTimeLabel.push(m52);
			startTimeLabel.push(m54);
			startTimeLabel.push(m56);
			startTimeLabel.push(m58);
			
			if(j==24){ j = 0}
		};

		var perShapeGradient = {
			x1 : 0,
			y1 : 0,
			x2 : 1,
			y2 : 0
		};
		colors = Highcharts.getOptions().colors;
		colors = [ {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(100,238,92 )' ], [ 1, 'rgb(100,238,92 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(250,210,80 )' ], [ 1, 'rgb(250,210,80 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(231,71,79 )' ], [ 1, 'rgb(231,71,79 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, '#8C9089' ], [ 1, '#8C9089' ] ]
		}, ]

		var height = window.innerHeight;

		var options = {
			chart : {
				type : 'coloredarea',
				backgroundColor : 'rgba(0,0,0,0)',
				height : getElSize(200),
				marginTop : -getElSize(300),
		    	marginRight : getElSize(50),
		    	marginLeft : getElSize(50)
			},
			credits : false,
			title : false,
			xAxis : {
				categories : startTimeLabel,
				labels : {
					step: 1,
					formatter : function() {
						var val = this.value

						return val;
					},
					style : {
						color : "white",
						fontSize : getElSize(30),
						fontWeight : "bold"
					},
				}
			},
			yAxis : {
				labels : {
					enabled : false,
				},
				title : {
					text : false
				},
			},
			tooltip : {
				enabled : false
			},
			plotOptions : {
				line : {
					marker : {
						enabled : false
					}
				}
			},
			legend : {
				enabled : false
			},
			series : []
		};

		$("#" + id).highcharts(options);

		var status = $("#" + id).highcharts();
		var options = status.options;

		options.series = [];
		options.title = null;
		options.exporting = false;
		
		getTimeData(options, today);
		
		///////////////////////// demo data
		
		
//		options.series.push({
//			data : [ {
//				y : Number(20),
//				segmentColor : "red"
//			} ],
//		});
	//	
	//	
//		for(var i = 0; i < 719; i++){
//			var color = "";
//			var n = Math.random() * 10;
//			if(n<=5){
//				color = "green";
//			}else if(n<=8){
//				color = "yellow";
//			}else {
//				color = "red";
//			}
//			options.series[0].data.push({
//				y : Number(20),
//				segmentColor : color
//			});
//		};
	//	
//		status = new Highcharts.Chart(options);
		
		
		
		////////////////////////////////
		
		console.log("drawBarchart");
		
	};
	
	var incycleColor = "#50BA29";
	var cuttingColor = "#175501";
	var waitColor = "yellow";
	var alarmColor = "red";
	var noconnColor= "#A0A0A0";
	
	function getTimeData(options, today){
		var url = ctxPath + "/getTimeData.do";
		
		if( dateDiff( moment($("#today").val()), moment().format("YYYY-MM-DD") ) >= 7 ){ // moment($("#today").val()).isBefore(moment().format("YYYY-MM-DD")) 
			url = ctxPath +"/getOldTimeData.do";
		}
		
		var param = "workDate=" + $("#today").val() + 
					"&dvcId=" + $("#dvcId").val() +
					"&YYMM=" + moment($("#today").val()).format("YYMM");
		
		//console.log("YYMM : " + moment($("#today").val()).format("YYMM"));
		//console.log(param)
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			data : param,
			success : function(data){
				// console.log("single data");
				// console.log(data)
				
				var json = data.statusList;
				
				var color = "";
				
				try{
					var status = json[0].status;
					var blank;
					var f_Hour = json[0].startDateTime.substr(11,2);
					var f_Minute = json[0].startDateTime.substr(14,2);
				}catch (e){
					console.log(e)					
				}
				
				if(status=="IN-CYCLE"){
					color = incycleColor;
				}else if(status=="WAIT"){
					color = waitColor;
				}else if(status=="ALARM"){
					color = alarmColor;
				}else if(status=="NO-CONNECTION"){
					color = noconnColor;
				};
				
				spdLoadPoint = [];
				spdOverridePoint = [];
				
				
				
				/* var startN = 0;
				if(f_Hour==startHour && f_Minute==(startMinute*10)){
					options.series.push({
						data : [ {
							y : Number(20),
							segmentColor : color
						} ],
					});
				}else{
					if(f_Hour>=Number(startHour)){
						startN = (((f_Hour*60) + Number(f_Minute)) - ((startHour*60) + (startMinute*10)))/2; 
					}else{
						startN = ((24*60) - ((startHour*60) + (startMinute*10)))/2;
						startN += (f_Hour*60/2) + (f_Minute/2);
					};
					
					options.series.push({
						data : [ {
							y : Number(20),
							segmentColor : noconnColor
						} ],
					});
						
					for(var i = 0; i < startN-1; i++){
						options.series[0].data.push({
							y : Number(20),
							segmentColor : noconnColor
						});
						spdLoadPoint.push(Number(0));
						spdOverridePoint.push(Number(0));
					};
				}; */
				
				var startN = 0;
				/* if(f_Hour==startHour && f_Minute==(startMinute*10)){
					options.series.push({
						data : [ {
							y : Number(20),
							segmentColor : color
						} ],
					});
				}else{
					options.series.push({
						data : [ {
							y : Number(20),
							segmentColor : noconnColor
						} ],
					});
					
					
					startN = ((f_Hour*60) + Number(f_Minute)) /2 
					
					for(var i = 0; i < startN-1; i++){
						options.series[0].data.push({
							y : Number(20),
							segmentColor : noconnColor
						});
						spdLoadPoint.push(Number(0));
						spdOverridePoint.push(Number(0));
					};
				} */
				
				options.series.push({
					data : [ {
						y : Number(20),
						segmentColor : color
					} ],
				});
				
				
				$(json).each(function(idx, data){
					spdLoadPoint.push(Number(data.spdLoad));
					spdOverridePoint.push(Number(data.spdOverride));

					if(data.status=="IN-CYCLE" && data.spdLoad != "0.0"){
						color = cuttingColor;
					}else if(data.status=="IN-CYCLE"){
						color = incycleColor;
					}else if(data.status=="WAIT"){
						color = waitColor;
					}else if(data.status=="ALARM"){
						color = alarmColor;
					}else if(data.status=="NO-CONNECTION"){
						color = noconnColor;
					};
					
					options.series[0].data.push({
						y : Number(20),
						segmentColor : color
					});
					
					
					
					
					
					// 엑셀 저장 excel
					excelData = "startDateTime,status,spdLoad,spdOverrideLINE";
					excelJson = [];
					excelJson = json;
					
					/* console.log("json");
					console.log(json);
					
					console.log("excelJson");
					console.log(excelJson); */

					var excelLength = excelJson.length;

					/* for(var i=0; i<excelWcList.length; i++){
						
						if(excelWcList[i][0].indexOf('______') == 0){
							excelLength = i;
							console.log("______ 의 최초 위치 : " + excelLength);
							break;
						}	
					} */

					for(var i = 0; i < excelLength; i++){	
						
						excelData += " " + excelJson[i].startDateTime  + ","
									+ excelJson[i].status + ","
									+ excelJson[i].spdLoad + ","
									+ excelJson[i].spdOverride + "LINE"
						
					};
					
					//console.log(excelData);
					 
					
					
					
					
					
					
					
				});
				
				for(var i = 0; i < 719-(json.length); i++){
					options.series[0].data.push({
						y : Number(20),
						segmentColor : "rgba(0,0,0,0)"
					});
					//spdLoadPoint.push(Number(-10));
					//spdOverridePoint.push(Number(-10));
				};
				
				status = new Highcharts.Chart(options);
				getSpdFeed();
				
				//drawLabelPoint();
			},error : function(e1,e2,e3){
			}
		});
	};
	
	function dateDiff(_date1, _date2) {
	    var diffDate_1 = _date1 instanceof Date ? _date1 : new Date(_date1);
	    var diffDate_2 = _date2 instanceof Date ? _date2 : new Date(_date2);
	 
	    diffDate_1 = new Date(diffDate_1.getFullYear(), diffDate_1.getMonth()+1, diffDate_1.getDate());
	    diffDate_2 = new Date(diffDate_2.getFullYear(), diffDate_2.getMonth()+1, diffDate_2.getDate());
	 
	    var diff = Math.abs(diffDate_2.getTime() - diffDate_1.getTime());
	    diff = Math.ceil(diff / (1000 * 3600 * 24));
	 
	    return diff;
	};
	
	var spdLoadPoint = [];
	var spdOverridePoint = [];
	
	function getSpdFeed(){
		$('#spdFeed').highcharts({
	        chart: {
	            type: 'line',
	            backgroundColor : "rgba(0,0,0,0)",
	            height : getElSize(380),
	          //  width : getElSize(1000)
	        },
	        exporting : false,
	        credits : false,
	        title: {
	            text: null
	        },
	        subtitle: {
	            text: null
	        },
	        legend : {
	        	enabled : false,
	        	itemStyle : {
	        			color : "white",
	        			fontWeight: 'bold'
	        		}
	        },
	        xAxis: {
	        	labels : {
	        		enabled : false
	        	}
	        },
	        yAxis: [{
	        	min : 0,
	        	gridLineWidth: 1,
				minorGridLineWidth: 0,
	            title: {
	                text: null
	            },
	            labels : {
	            	 useHTML: true,
	                 formatter: function() {
	                     return "<span style='color:red; background-color:black; padding:" + getElSize(10) + "px'>" + this.value + "</span>";
	                 },
	            	x: getElSize(50),
	            }
	        },{
	        	min : 0,
	        	opposite: true,
	        	gridLineWidth: 1,
	            title: {
	                text: null
	            },
	            labels : {
	            	useHTML: true,
	            	formatter: function() {
	                     return "<span style='color:yellow; background-color:black; padding:" + getElSize(10) + "px'>" + this.value + "</span>";
	                 },
	            	x: -getElSize(50),
	            }
	        }],
	        plotOptions: {
	            line: {
	                dataLabels: {	
	                    enabled: false,
	                    color: 'white',
	                    style: {
	                        textShadow: false 
	                    }
	                },
	                enableMouseTracking: false
	            }
	        },
	        series: [{
	            		name: 'Spindle Load',
	            		color : 'yellow',
	            		data : spdLoadPoint,
	            		lineWidth : getElSize(10),
	            		marker : {
                    			enabled : false,
                		},
                		yAxis: 1
	        		},
	        		{
	            		name: 'Spindle Override',
	            		color : 'red',
	            		data: spdOverridePoint,
	            		lineWidth : getElSize(10),
	            		marker : {
                    		enabled : false,
                		},
	        		},
	        		{
	            		name: '',
	            		color : 'rgba(0,0,0,0)',
	            		data: tmpPoint,
	            		lineWidth : getElSize(10),
	            		marker : {
                    		enabled : false,
                		},
	        		}]
	    });	
		
		spdChart = $('#spdFeed').highcharts();
		$("#legend").css("display", "inline");
	};
	
	function drawPrdctChart(ratio){
		$("#target_ratio").html(Math.round(ratio) + "%").css({
			"color" : "#0080FF",
			"font-size" : getElSize(100),
			"right" : getElSize(30),
			"top" : getElSize(850)
		});
		
		$('#prdctChart').highcharts({
		    chart: {
		    	backgroundColor : "rgba(0,0,0,0)",
		    	height : getElSize(230),
		    	marginRight : getElSize(50),
		    	marginLeft : getElSize(50),
		        type: 'bar'
		    },
		    credits : false,
		    exporting : false,
		    title: {
		        text: null
		    },
		    xAxis: {
		    	/* gridLineWidth:0,
		    	lineWidth: 0,*/
		    	tickWidth: 0, 
				labels: {
		       		enabled: false
		    	},    
		    },
		    yAxis: {
			    gridLineWidth: 0,
				minorGridLineWidth: 0,
		        min: 0,
		        max :100,
				labels : {
					style : {
						color : "#7A7B7C",
						fontSize :getElSize(30)
					}
		        },
		        title: {
		            text: null
		        }
		    },
		    tooltip : {
				enabled : false
			},
		    legend: {
		        reversed: true,
		        enabled : false
		    },
		    plotOptions: {
		        series: {
		            stacking: 'normal',
		            groupPadding: 0,
		            pointPadding: 0,
		            borderWidth: 0
		        }
		    },
		    /* series: [{
		    		color : "#BBBDBF",
		        name: 'target',
		        data: [10]
		    },{
		    		color : "#A3D800",
		        name: 'goal',
		        data: [10]
		    }] */
		    series: [{
	    		color : "#A3D800",
	        name: 'goal',
	        data: [Math.round(ratio)]
	    }]
		});			
	};
	
	function getAlarmList(){
		//console.log("get alarm")
		var url = "${ctxPath}/getAlarmList.do";
		var param = "dvcId=" + dvcId + 
					"&sDate=" + $("#today").val() + 
					"&eDate=" + $("#today").val() + " 23:59:59";
	
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
	
				var alarm = "";
				$(json).each(function(idx, data){

					//$("#alarmCode1_" + i).html("[" + data.startDateTime.substr(5,14)+"] " + data.alarmCode + " - ");
					//$("#alarmMsg1_" + i).html(data.alarmMsg);
					
					var alarm1, alarm2, alarm3;
					if(data.ncAlarmNum1=="" || data.ncAlarmNum1==null){
						alarm1 = "";
					}else{
						alarm1 = "[" + data.startDateTime.substr(5,14) + "] - " +  decodeURIComponent(data.ncAlarmMsg1).replace(/\+/gi, " ")  + "<br>";
						//alarm1 = data.ncAlarmNum1 + " - " +  decodeURIComponent(data.ncAlarmMsg1).replace(/\+/gi, " ") + "<br>";
					};
					
					if(data.ncAlarmNum2=="" || data.ncAlarmNum2==null){
						alarm2 = "";
					}else{
						//alarm2 = data.ncAlarmNum2 + " - " +  decodeURIComponent(data.ncAlarmMsg2).replace(/\+/gi, " ") + "<br>";
						alarm2 = "[" + data.startDateTime.substr(5,14) + "] - " +  decodeURIComponent(data.ncAlarmMsg2).replace(/\+/gi, " ")  + "<br>";
					};
					
					if(data.ncAlarmNum3=="" || data.ncAlarmNum3==null){
						alarm3 = "";
					}else{
						//alarm3 = data.ncAlarmNum3 + " - " +  decodeURIComponent(data.ncAlarmMsg3).replace(/\+/gi, " ") + "<br>";
						alarm3 = "[" + data.startDateTime.substr(5,14) + "] - " +  decodeURIComponent(data.ncAlarmMsg3).replace(/\+/gi, " ")  + "<br>";
					};
					
					
					alarm += alarm1 +
							alarm2 +
							alarm3; 
							
					if(data.status=="ALARM" && alarm == ""){
						//console.log("get")
						alarm = "[장비 확인 필요]";
					}
					
				});
	
				if(alarm!=""){
					alarm = "<font style='color:red' id='alarm_span'>-Alarm</font><br><div style='height:" + getElSize(250) + "px; overflow:auto;'>" + alarm + "</div>";
				}
				
				$("#alarm_div").html(alarm).not("#alarm_span").css({
					"color" : "white",
				//	"padding" : getElSize(50),
					"margin" : getElSize(20) + "px"
				});
			}
		});

	};
	
	function getDetailData(){
		var url = ctxPath + "/getDetailBlockData.do";
		var param = "dvcId=" + dvcId + 
					"&sDate=" + $("#today").val();
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				
				var json = data.dataList;				
				var alarm = "";
				
				getAlarmList();
				if(json.length==0){
					$("#cylCnt").html("-");
					$("#prdctPerCyl").html("-");
					$("#daily_target_cycle").html("-");
					$("#complete_cycle").html("-");
					$("#daily_avg_cycle_time").html("-");
					$("#daily_length").html("-");
					$("#feedOverride").html("-");
					$("#spdLoad").html("-");
					$("#downValue").html("-");
				};
				
				$(json).each(function(idx, data){
					var hour = new Date().getHours()-8;
					if(hour==0) hour=1;
					 
					var sign = "";
					
					$("#cylCnt").html(Math.round(data.lastFnPrdctNum/data.prdctPerCyl));
					$("#prdctPerCyl").html(data.prdctPerCyl);
					$("#daily_target_cycle").html(data.tgCnt);
					$("#complete_cycle").html(data.lastFnPrdctNum);
					
					/* if(data.type == 'IOL'){
						$("#complete_cycle").html('NA');	
					}else{
						$("#complete_cycle").html(data.lastFnPrdctNum);
					} */
					
					$("#daily_avg_cycle_time").html(Number(data.LastAvrCycleTime/60).toFixed(1));
					$("#daily_length").html(Number(data.prdctPerHour).toFixed(1));
					$("#feedOverride").html(Number(data.feedOverride));
					$("#spdLoad").html(Number(data.spdLoad));
					
					//var remainCnt = data.remainCnt;
					var remainCnt = data.tgCnt - (data.lastFnPrdctNum);
					var color = "";
					if(Number(remainCnt)>0){
						sign = "-";
						color = "red"
					}else{
						sign = "+";
						remainCnt = Math.abs(remainCnt);
						color = "blue";
					};
					
					$("#downValue").html(sign + remainCnt).css({
						"font-size" : getElSize(70),
						"color" : color
					})
					
					
					//일 생산 현황
					//drawPrdctChart(data.lastFnPrdctNum / data.tgCnt * 100);
					/* var prdctChart = $("#gauge").highcharts().series[0].points[0];
					prdctChart.update(Number(Number(data.lastFnPrdctNum / data.tgCnt * 100).toFixed(1))); */
					
					
					$("#gauge").css({
					}).empty();
					
					
					
					var prdRatio = Number(Number(data.lastFnPrdctNum / data.tgCnt * 100).toFixed(1));
					var bckColor = "#353535"
					
					if(data.tgCnt==0){
						prdRatio = 0
					}
					
					if (prdRatio > 100){
						//prdRatio = 100;
						bckColor = '#0080FF';
					}
					
					$("#gauge").circleDiagram({
						textSize: getElSize(70), // text color
						percent : prdRatio + "%",
						size: getElSize(300), // graph size
						borderWidth: getElSize(50), // border width
						bgFill: bckColor,//"#353535", // background color
						frFill: "#0080FF", // foreground color
						//font: "serif", // font
						textColor: '#0080FF' // text color
					});
					
					$("#gauge").css("display", "block")
					var n = (data.lastFnPrdctNum)/data.tgCnt*100;
					//barChart.series[0].data[0].update(Number(Number(n).toFixed(1)));
					
					if(String(data.type).indexOf("IO")!=-1){
						$("#gauge").css("display", "none")
						$("#cylCnt").html("-");
						$("#prdctPerCyl").html("-");
						$("#daily_target_cycle").html("-");
						$("#complete_cycle").html("-");
						$("#daily_avg_cycle_time").html("-");
						$("#daily_length").html("-");
						$("#feedOverride").html("-");
						$("#spdLoad").html("-");
						$("#downValue").html("-");
					}
				});
				
				
				$("#downValue").css({
					"position" : "absolute",
					"top" : getElSize(350)
				});

				$("#downValue").css({
					"left" : getElSize(1130),
				});
				
				//setTimeout(getDetailData, 5000)
			}
		});
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setDivPos(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			//"background-color" : "black",
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			//"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		
		//console.log("lang2 : " + getParameterByName('lang'));
		
		if(getParameterByName('lang')=='ko'){
			$("#title").css({
				"color" : "white",
				"font-size" : getElSize(100),
				"position" : "absolute",
				"top" : marginHeight + getElSize(85),
				"z-index" : 9997,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(getParameterByName('lang')=='en' || getParameterByName('lang')=='de'){
			$("#title").css({
				"color" : "white",
				"font-size" : getElSize(70),
				"position" : "absolute",
				"top" : marginHeight + getElSize(115),
				"z-index" : 9997,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else{
			$("#title").css({
				"color" : "white",
				"font-size" : getElSize(90),
				"position" : "absolute",
				"top" : marginHeight + getElSize(100),
				"z-index" : 9997,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}
		
		
		
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 3,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(35),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$(".content").css({
			"height" : getElSize(400),
			"background-color" : "#191919",
			"color" : "white",
			"text-align" : "center",
			"font-size" : getElSize(170),
		});
		
		$("#prdctChart_td").css({
			"height" : getElSize(400),
			"background-color" : "#191919",
		});
		
		$("#timeChart_td").css({
			"height" : getElSize(700),
			"background-color" : "#191919"
		});
	
		$("#timeChart").css({
			"margin-top" : getElSize(50)	
		});
		
		$("#dvcId").css({
			//"position" : "absolute",
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20),
			//"top" : getElSize(160),
			//"margin-top" : getElSize(25),
			//"margin-left" : getElSize(30)
		});
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
			"position" : "relative",
			"top" : getElSize(26)
		});
		
		$("#statusChart").css({
			"z-index" : 9997,
			"position" : "absolute",
			"left" : getElSize(540),
			"width" : getElSize(750),
			"top" : getElSize(1000)
		});
		
		$("#legend").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"margin-left" : getElSize(1600),
			"display" : "none"
		});
		
		$("#spdLoadLine").css({
			"background-color" : "#F9FF00",			
			"width" : getElSize(90),
			"height" : getElSize(20)
		});
		
		$("#spdOvrrdLine").css({
			"background-color" : "#FF0000",
			"width" : getElSize(90),
			"height" : getElSize(20),
			"margin-left" : getElSize(50)
		});
		
		$("#cutTime, #inCycleTime, #waitTime, #alarmTime, #noConnTime").css({
			"color" : "black",
			"z-index" : 9997,
			"font-size" : getElSize(35)
		});
		
				
		$("#inCycleTime").css({		
			"top" : getElSize(1015) ,
			"left" : getElSize(960)
		});
		
		$("#cutTime").css({
			"top" : getElSize(1077) ,
			"left" : getElSize(960)
		});
		
		$("#waitTime").css({
			"top" : getElSize(1138) ,
			"left" : getElSize(960)
		});
		
		$("#alarmTime").css({
			"top" : getElSize(1203) ,
			"left" : getElSize(960)
		});
		
		$("#noConnTime").css({
			"top" : getElSize(1267) ,
			"left" : getElSize(960)
		});
		
		$("#cutSpan, #inCycleSpan, #waitSpan, #alarmSpan, #noConnSpan").css({
			"z-index" : 999999,
			"color" : "black",
			"font-size" : getElSize(35),
			"position" : "absolute"
		});
		
		$("#inCycleSpan").css({
			"top" :  getElSize(1015) ,
			"left" : getElSize(1110)
		});
		
		$("#cutSpan").css({
			"top" : getElSize(1075) ,
			"left" : getElSize(1110)
		});
		
		$("#waitSpan").css({
			"top" : getElSize(1135) ,
			"left" : getElSize(1110)
		});
		
		$("#alarmSpan").css({
			"top" : getElSize(1200) ,
			"left" : getElSize(1110)
		});
		
		$("#noConnSpan").css({
			"top" : getElSize(1263) ,
			"left" : getElSize(1110)
		});
		
		$("#opratio_gauge").css({
			"margin-left" : getElSize(100),
			"margin-top" : getElSize(670)
		});
		
		$("#cutting_gauge").css({
			"margin-right" : getElSize(100),
			"margin-top" : getElSize(670)
		});
		$("img").css({
			"display" : "inline"
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(100),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#date_table").css({
			"position" : "absolute",
			"top" :getElSize(228) + marginHeight,
			"left" : getElSize(504) + marginWidth,
			"color" : "#BFBFBF",
			
			"font-size" : getElSize(50),
			"z-index" : 9997
		});
		 
		/* $("#date_table td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		}); */
		
		$("#date_table button").css({
			"width" : getElSize(100),
			"height" : getElSize(60)
		});
		
		$("#today").css({
			"font-size" : getElSize(40) + "px",
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20),
			//"height" : getElSize(80)
		})
		
		$("#pieChart").css({
			"z-index" : -20,
			"position" : "absolute",
			"width" : getElSize(500) + "px",
			"height" : getElSize(500) + "px",
			"z-index" : 9997,
			//"top" : marginHeight + getElSize(1115) + "px",
			"top" : $("#statusChart").offset().top + getElSize(10),
			"left" : $("#statusChart").offset().left + getElSize(5)
		});
		
		$("#pieCuttingChart").css({
			"z-index" : -20,
			"position" : "absolute",
			//"width" : getElSize(500) + "px",
			//"height" : getElSize(500) + "px",
			"z-index" : 9997,
			//"top" : marginHeight + getElSize(1115) + "px",
			"top" : $("#statusChart").offset().top + getElSize(12),
			"left" : $("#statusChart").offset().left + getElSize(6)
		});
		
		//pieChartStatus
		$("#pieChartStatus").css({
			"z-index" : -20,
			"position" : "absolute",
			//"width" : getElSize(500) + "px",
			//"height" : getElSize(500) + "px",
			"z-index" : 9998,
			//"top" : marginHeight + getElSize(1115) + "px",
			"top" : $("#statusChart").offset().top + getElSize(12),
			"left" : $("#statusChart").offset().left + getElSize(6),
			//"opacity" : 0.8
		});
		
		$("#app_store_iframe").css({
			"width" : getElSize(3100) + "px",
			"height" : getElSize(2000) + "px",
			"position" : "absolute",
			"z-index" : 9999,
			"display" : "block"
		});
		
// 		$("#app_store_iframe").css({
// 			"left" : originWidth * 1.5,
// 			"top" : (originHeight/2) - ($("#app_store_iframe").height()/2) 
// 		}).attr("src", appServerUrl)
		
		
		$("#delDiv").css({
			"color" : "white",
			"z-index" : 9999999,
			"background-color" : "black",
			"position" : "absolute",
			"width" : getElSize(700) + "px",
			"font-size" : getElSize(60) + "px",
			"text-align" : "center",
			"padding" : getElSize(30) + "px",
			"border" : getElSize(7) + "px solid rgb(34,34,34)",
			"border-radius" : getElSize(50) + "px"
		});
		
		
		$("#delDiv div").css({
			"background-color" : "rgb(34,34,34)",
			"padding" : getElSize(20) + "px",
			"margin" : getElSize(10) + "px",
			"cursor" : "pointer"
		}).hover(function(){
			$(this).css({
				"background-color" : "white",
				"color" : "rgb(34,34,34)",
			})
		}, function(){
			$(this).css({
				"background-color" : "rgb(34,34,34)",
				"color" : "white",
			})
		});
		
		$("#delDiv").css({
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			"top" : - $("#delDiv").height() * 2
		});
		
		$("#delDiv div:nth(1)").click(function(){
			$("#delDiv").animate({
				"top" : - $("#delDiv").height() * 2
			});
		});
		
		$("#up, #down").css({
			"width" : getElSize(70) *  1.2,
			"height" : getElSize(70),		
		})
		
		$("#excel").css({
			//"color" : "white",
			"font-size" :getElSize(30),
			"position" : "absolute",
			//"z-index" : 9,
			"top" : getElSize(1265),
			"left" : getElSize(3680),
			"width" : getElSize(140),
			"height" : getElSize(56)
		})
	};
		
	
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	$.datepicker.setDefaults({
	    dateFormat: 'yy-mm-dd',
	    prevText: '이전 달',
	    nextText: '다음 달',
	    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
	    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
	    dayNames: ['일', '월', '화', '수', '목', '금', '토'],
	    dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
	    dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
	    showMonthAfterYear: true,
	    yearSuffix: '년'
	});
	
	$(function() {
	    	/* $("#today").datepicker({
	    		onSelect:function(){
	    			console.log("select")
	    			changeDateVal()
	    		}
	    	}) */
	    	
	});
	
	var excelData;

	function csvSend(){
		var id = this.id;
		var sDate, eDate;
		var csvOutput;
		
		sDate = today;
		eDate = today + " 23:59:59";
		csvOutput = excelData;
		
		csvOutput = csvOutput.replace(/\n/gi,"");
		
		console.log("dvcId : " + $("#dvcId option:selected").text());
		
		$("#excelTitle").val("Single_Chart_Status");
		
		f.csv.value=encodeURIComponent(csvOutput);
		f.startDate.value = $("#dvcId option:selected").text();
		f.endDate.value = eDate;
		f.submit();
	};
	
</script>
</head>
<body>

	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
		<input type="hidden" name="title" id="excelTitle">
	</form>

	<div id="delDiv">
		<spring:message  code="chk_del"></spring:message>
		<div><spring:message  code="check"></spring:message></div>
		<div><spring:message  code="cancel"></spring:message></div>
	</div>
	
<!-- 	<iframe id="app_store_iframe"  style="display: none"></iframe> -->
	
	<div id="pieChart"></div>
	<div id="pieCuttingChart"></div>
	<div id="pieChartStatus"></div>
	
	<div id="programName"></div>
	<div id="time"></div>
	<div id="title"><spring:message code="dailydevicestatus_title"></spring:message></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<div id="date_table">
				&nbsp;&nbsp;
				<spring:message code="device"></spring:message>
				<select id="dvcId"></select>
				<spring:message code="op_period"></spring:message>
				<button id="up">▲︎</button><button id="down">▼</button>
				<input type="date" class="date" id="today">
				<img alt="" src="${ctxPath }/images/search.png" id="search" onclick="changeDateVal()">
			</div>
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" style="display: none">
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right' style="display: none">
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/monitor_left.png" class='menu_left'  style="display: none">
				</td>
				<td >
					
					
					
					<%-- &nbsp;&nbsp;
					<spring:message code="device"></spring:message>
					<select id="dvcId"></select> --%>
					<!-- <table id="date_table">
						<tr>
							<td>
								<button id="up">▲︎</button><button id="down">▼</button>
							</td>
							<td>
								<input type="date" id="today" >		
							</td>
						</tr>
					</table> -->
					<img alt="" src="${ctxPath }/images/blue_right.png" class='menu_right' style="display: none">
				</td>
			</tr>
			<Tr>
				<Td>
					<span  class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table  style="width: 100%" id="main_table">
						<tr>
							<td style="text-align: center; width: 25%;white-space: nowrap" class="title_span" colspan="2">
								<spring:message code="dailyprdcttarget"></spring:message>	
							</td>
							<td style="text-align: center; width: 25%;white-space: nowrap" class="title_span" colspan="2">
								<spring:message code="dailyprdctcnt"></spring:message>	
								<!-- <span id="downValue"></span> -->
							</td>
							<%-- <td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								<spring:message code="prdct_per_cycle"></spring:message>	
							</td>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								<spring:message code="daily_cycle_cnt"></spring:message>	
							</td>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								<spring:message code="daily_prdct_avrg_cycle_time"></spring:message>	
							</td>
							<td style="text-align: center; width: 12.5%;white-space: nowrap" class="title_span">
								<spring:message code="daily_prdct_cnt_per_hour"></spring:message>	
							</td> --%>
							<td style="text-align: center; width: 25%;white-space: nowrap" class="title_span" colspan="2">
								<spring:message code="feed_overrde"></spring:message>	
							</td>
							<td style="text-align: center; width: 25%;white-space: nowrap" class="title_span" colspan="2">
								<spring:message code="spd_load"></spring:message>	
							</td>
						</tr>
						<tr>
							<td class='content' id="daily_target_cycle" colspan="2"></td>
							<td class='content' id="complete_cycle" colspan="2"></td>
							<!-- <td class='content' id="prdctPerCyl"></td>
							<td class='content' id="cylCnt"></td>
							<td class='content' id="daily_avg_cycle_time"></td>
							<td class='content' id="daily_length"></td> -->
							<td class='content' id="feedOverride" colspan="2"></td>
							<td class='content' id="spdLoad" colspan="2"></td>
						</tr>
						<tr>
							<TD class="title_span" colspan="2" >
								<spring:message code="dailystackedstatus"></spring:message>	
							</TD>
							<TD class="title_span" colspan="2" >
								<spring:message code="prdct_status"></spring:message>	
							</TD>
							<TD class="title_span" colspan="4" >
								<spring:message code="msg"></spring:message>
							</TD>
						</tr>
						<Tr>
							<td rowspan="3" colspan="2" style="text-align: center; vertical-align: top;" >
								<!-- <span id="cutTime">Cut</span>  --><span id="cutSpan">0</span>
								<%-- <span id="inCycleTime"><spring:message code="incycle"></spring:message></span>  --%><span id="inCycleSpan">0</span>
								<%-- <span id="waitTime"><spring:message code="wait"></spring:message></span>  --%><span id="waitSpan">0</span>
								<%-- <span id="alarmTime"><spring:message code="stop"></spring:message></span>  --%><span id="alarmSpan">0</span>
								<%-- <span id="noConnTime"><spring:message code="noconnection_board"></spring:message></span>  --%><span id="noConnSpan">0</span>
								
								<img alt="" src="${ctxPath }/images/statusChart.png" id="statusChart">
								
								<div id="opratio_gauge" style="float: left;"></div>
								<div id="cutting_gauge" style="float: right;"></div>
								<span id="opratio_span"></span>
								<span id="cuttingratio_span"></span>
								
							</td>
							<td colspan="2" style="text-align: center; vertical-align: bottom;" id="prdctChart_td">
								<!-- <span id="target_ratio" ></span>
								<div id="prdctChart" ></div> -->
								<center>
									<div id="gauge"></div>
								</center> 
							</td>
							<td colspan="4" style="background-color: rgb(25,25,25); vertical-align: top;" >
								<!-- <div id="program_div"></div> -->
								<div id="alarm_div"></div>
							</td>
						</Tr>
						<tr>
							<TD class="title_span" colspan="6">
								<spring:message code="operation_status"></spring:message>	
							</TD>
						</tr>
						<tr>
							<td colspan="6" style="text-align: center; vertical-align: top;" id="timeChart_td">
								<div id="timeChart"></div>
								<button onclick="csvSend()" id="excel"> EXCEL </button>
								<div id="spdFeed"></div>
								<table id="legend">
									<Tr>
										<td ><div id="spdOvrrdLine"></div></td>
										<Td>Spindle Override</Td>
										<td><div id="spdLoadLine"></div></td>
										<Td><spring:message code="spd_load"></spring:message></Td>
										
									</Tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span  class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span  class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span  class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span  class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span  class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span  class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
		</table>
	 </div>
	
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	
